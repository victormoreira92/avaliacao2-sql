USE Avaliacao;
/*Inserir dados na tabela*/
INSERT INTO `USUARIO` (`idUsuario`, `idIdentificacao`, `admim`) VALUES
	('12', '215', '0'),
	('13', '216', '0'),
	('14', '217', '0'),
	('15', '218', '1'),
	('16', '219', '0'),
	('17', '220', '0'),
	('18', '221', '0'),
	('19', '222', '0'),
	('20', '223', '0');
    
INSERT INTO `IDENTIFICACAO` (`idIdentificacao`, `nome`, `cpf`) VALUES
	('12', 'Vanessa Rodrigues', '71368758541'),
	('13', 'Alexandre da Rosa', '71368758542'),
	('14', 'Emanuelly da Rosa', '71368758543'),
	('15', 'Ana Beatriz Costa', '71368758544'),
	('16', 'Mariana Cunha', '71368758545'),
	('17', 'João Vitor Mendes', '71368758546'),
	('18', 'Levi Gonçalves', '71368758547'),
	('19', 'Emanuelly Lopes', '71368758548'),
	('20', 'Gustavo Fogaça', '71368758549');


INSERT INTO `CARONA` (`idUsuario`, `idCarona`, `partida`, `destino`, `data_hora`, `passageiros`) VALUES
	('12', '321', 'Penha', 'Tijuca', '2021-01-23 15:29:15', '4'),
	('13', '322', 'Sao Goncalo', 'Itaborai', '2019-01-07 20:21:39', '3'),
	('14', '323', 'Nova Iguacu', 'Sao Goncalo', '2020-02-13 01:17:25', '4'),
	('15', '324', 'Duque de Caxias', 'Sao Goncalo', '2020-02-14 01:17:25', '3'),
	('16', '325', 'Meier', 'Japeri', '2020-02-15 01:17:25', '3'),
	('17', '326', 'Meier', 'Sao Goncalo', '2020-02-16 01:17:25', '1'),
	('18', '327', 'Meier', 'Vila Penha', '2022-06-05 07:05:28', '4'),
	('19', '328', 'Vigario Geral', 'Sao Goncalo', '2022-04-24 06:03:34', '1'),
	('20', '329', 'Vigario Geral', 'Queimados', '2022-10-25 06:03:34', '3'),
	('12', '330', 'Vigario Geral', 'Belford Roxo', '2019-11-26 06:03:34', '4'),
	('15', '331', 'Sao Goncalo', 'Nilopolis', '2019-09-27 06:03:34', '2'),
	('17', '332', 'Sao Goncalo', 'Angra dos Reis', '2021-01-14 06:28:15', '4'),
	('19', '333', 'Sao Goncalo', 'Parati', '2023-01-21 02:50:10', '4'),
	('16', '334', 'Sao Goncalo', 'Angra dos Reis', '2023-01-15 19:25:40', '3');

INSERT INTO `PARADA` (`idParada`, `idCarona`, `descricao`) VALUES
	('421', '321', 'rua dos filmes'),
	('422', '322', 'rua dos filmes'),
	('423', '323', 'rua dos criancas'),
	('424', '324', 'rua dos relatos'),
	('425', '325', 'rua dos podcast'),
	('426', '326', 'rua dos flores'),
	('427', '327', 'rua dos mercados'),
	('428', '328', 'rua dos radios'),
	('429', '329', 'rua dos mercados'),
	('430', '330', 'rua dos mercados'),
	('431', '331', 'rua dos invencao'),
	('432', '332', 'rua dos invencao'),
	('433', '333', 'rua dos ingles'),
	('434', '334', 'rua dos yent');