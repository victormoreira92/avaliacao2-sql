USE Avaliacao;

/*Mudando os dados para retornar resultados nas buscas*/
UPDATE `CARONA`
SET `destino`= REPLACE(`destino`,'Sao Goncalo','Praia Vermelha');

UPDATE `PARADA`
SET `descricao`= REPLACE(`descricao`,'rua dos filmes','Plaza Shopping');

UPDATE `CARONA`
SET `data_hora`= REPLACE(`data_hora`,'2020-02-13 01:17:25', '2022-08-13 01:17:25');


/*1 - Quais as caronas com destino Praia Vermelha a partir do dia 01/07/2022?*/
SELECT * FROM Avaliacao.CARONA WHERE 
destino= 'Praia Vermelha' AND
data_hora >=  '2022-07-01';

/*2 - Quais os nomes dos usuários que disponibilizam caronas com partida em São Gonçalo?*/
SELECT NOME FROM IDENTIFICACAO
INNER JOIN CARONA ON 
IDENTIFICACAO.idIdentificacao = CARONA.idUsuario 
WHERE CARONA.partida = 'Sao Goncalo'
ORDER BY IDENTIFICACAO.idIdentificacao;

/*3 - Quais os nomes dos usuários que disponibilizam
 caronas com pelo menos 2 passageiros e com parada no Plaza Shopping?*/

SELECT nome, passageiros FROM IDENTIFICACAO INNER JOIN (
	SELECT idUsuario, passageiros FROM Avaliacao.CARONA INNER JOIN Avaliacao.PARADA ON 
	CARONA.idCarona = PARADA.idCarona
	WHERE CARONA.passageiros >= 2 AND PARADA.descricao = 'Plaza Shopping'
) AS BUSCA ON IDENTIFICACAO.idIdentificacao = BUSCA.idUsuario; 
