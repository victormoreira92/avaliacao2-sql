/*Criacao do database*/
CREATE SCHEMA IF NOT EXISTS Avaliacao;
USE Avaliacao;

/*Criacao das tabelas do banco de dados*/
CREATE TABLE IF NOT EXISTS `Avaliacao`.`USUARIO`  (
	`idUsuario` INTEGER NOT NULL,
    `idIdentificacao` INTEGER NOT NULL,
    `admim` BOOLEAN, 
	PRIMARY KEY(`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `Avaliacao`.`IDENTIFICACAO`  (
    `idIdentificacao` INTEGER NOT NULL, 
    `nome` VARCHAR(255) NOT NULL,
    `cpf` VARCHAR(11),
    PRIMARY KEY(`idIdentificacao`),
    CONSTRAINT `fk_usuario_identificacao` FOREIGN KEY (`idIdentificacao`)
    REFERENCES `USUARIO`(`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `Avaliacao`.`CARONA`(
	`idCarona` INTEGER NOT NULL, 
    `idUsuario` INTEGER NOT NULL, 
    `partida` VARCHAR(255),
    `destino` VARCHAR(255),
    `data_hora` DATETIME,
    `passageiros` INTEGER, 
    PRIMARY KEY (`idCarona`),
	CONSTRAINT `fk_carona_usuario` FOREIGN KEY (`idUsuario`)
    REFERENCES `USUARIO`(`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `Avaliacao`.`PARADA`  (
	`idParada` INTEGER NOT NULL,
    `idCarona` INTEGER,
    `descricao` varchar(255), 
	PRIMARY KEY(`idParada`),
	CONSTRAINT `fk_parada_carona` FOREIGN KEY (`idCarona`)
    REFERENCES `CARONA`(`idCarona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
